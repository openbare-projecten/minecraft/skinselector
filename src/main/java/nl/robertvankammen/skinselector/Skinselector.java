package nl.robertvankammen.skinselector;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

import static net.kyori.adventure.text.Component.text;
import static org.bukkit.event.inventory.InventoryType.CHEST;
import static org.bukkit.event.player.PlayerResourcePackStatusEvent.Status.SUCCESSFULLY_LOADED;

public final class Skinselector extends JavaPlugin implements Listener {
    private static final Set<Material> PICKAXES = Set.of(Material.WOODEN_PICKAXE, Material.STONE_PICKAXE, Material.IRON_PICKAXE, Material.GOLDEN_PICKAXE, Material.DIAMOND_PICKAXE, Material.NETHERITE_PICKAXE);
    private static final Map<UUID, Inventory> PLAYER_SHOPS = new HashMap<>();
    private static final String COMMAND_STRING = "skin";
    private final HashMap<UUID, PlayerResourcePackStatusEvent.Status> playerResourceStatus = new HashMap<>();
    private String resourceUrl = "";
    private String resourceHash = "";
    private List<Integer> pickaxeSkins;

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
        createConfig();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void createConfig() {
        resourceUrl = getConfig().getString("resourceUrl");
        resourceHash = getConfig().getString("resourceHash");
        if (resourceUrl == null) {
            resourceUrl = "https://download.mc-packs.net/pack/2bd50970218b20da69d24abcd391d33a1f5ab493.zip";
            resourceHash = "2bd50970218b20da69d24abcd391d33a1f5ab493";
            getConfig().set("resourceUrl", resourceUrl);
            getConfig().set("resourceHash", resourceHash);
            saveConfig();
        }
        pickaxeSkins = (List<Integer>) getConfig().getList("pickaxeSkins");
        if (pickaxeSkins == null) {
            getConfig().set("pickaxeSkins", List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 99));
            saveConfig();
            pickaxeSkins = (List<Integer>) getConfig().getList("pickaxeSkins");
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase(COMMAND_STRING) && sender instanceof Player player) {
            var itemInHand = player.getInventory().getItemInMainHand();
            if (PICKAXES.contains(itemInHand.getType())) {
                var inv = Bukkit.createInventory(null, CHEST, text("Selecteer je skin, kosten 1 dia"));
                for (int i = 0; i < pickaxeSkins.size(); i++) {
                    var pick = itemInHand.clone();
                    var meta = pick.getItemMeta();
                    meta.setCustomModelData(pickaxeSkins.get(i));
                    pick.setItemMeta(meta);
                    inv.setItem(i, pick);
                }
                player.openInventory(inv);
                PLAYER_SHOPS.put(player.getUniqueId(), inv);
            } else {
                player.sendMessage(Component.text("De item die je in je hand houd wordt niet ondersteunt"));
            }
        }
        return false;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent playerJoinEvent) {
        playerJoinEvent.getPlayer()
                .setResourcePack(resourceUrl, resourceHash, true, null);
    }

    @EventHandler
    public void playerResourcePack(PlayerResourcePackStatusEvent event) {
        var uuid = event.getPlayer().getUniqueId();
        var status = event.getStatus();
        playerResourceStatus.put(uuid, status);
    }

    @EventHandler
    public void entityDamage(EntityDamageEvent event) {
        var entityUuid = event.getEntity().getUniqueId();
        var status = playerResourceStatus.get(entityUuid);
        if (status != null && status != SUCCESSFULLY_LOADED) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInvetoryClick(InventoryClickEvent inventoryClickEvent) {
        var clickedInvetory = inventoryClickEvent.getClickedInventory();
        var player = inventoryClickEvent.getWhoClicked();
        var inv = PLAYER_SHOPS.get(player.getUniqueId());
        if (inv != null && inv.equals(clickedInvetory)) {
            inventoryClickEvent.setCancelled(true);
            var itemOffHand = player.getInventory().getItemInOffHand();
            if (itemOffHand.getType().equals(Material.DIAMOND)) {
                player.getInventory().setItemInMainHand(inv.getItem(inventoryClickEvent.getSlot()));
                itemOffHand.setAmount(itemOffHand.getAmount() - 1);
            } else {
                player.sendMessage(Component.text("Je moet diamonds in je off hand hebben (skinn veranderen kost 1 dia)"));
            }
            clickedInvetory.close();
        }
    }
}
